#!/bin/bash
# Audio Replace
# audio_replace.sh basefile replacementfile outfile replacementstarttime
# All times measured in samples.
rate=$(soxi -r "$1")      # sample rate
length=$(soxi -s "$2")    # length of replacement
excess=$[rate * 5 / 1000] # Using default excess
leeway=$excess            # and leeway.
before=`tempfile --suffix '.wav'`
after=`tempfile --suffix '.wav'`
intermediate=`tempfile --suffix '.wav'`
baselength=$(soxi -s "$1")
if [[ $[$4 + $excess] -gt $[2 * $excess] ]] ; then
    sox "$1" $before trim 0 $[$4 + $excess]s
fi
if [[ $[$4 + $length - $excess - $leeway] -le $baselength ]] ; then
    sox "$1" $after trim $[$4 + $length - $excess - $leeway]s
fi
if [[ $[$4 + $excess] -gt $[2 * $excess] ]] ; then
    sox $before "$2" $intermediate splice $[$4 + $excess]s
else
    cp "$2" $intermediate
fi
if [[ $[$4 + $length - $excess - $leeway] -le $baselength ]] ; then
    intermediatelength=$(soxi -s $intermediate)
    sox $intermediate $after "$3" splice ${intermediatelength}s
else
    mv $intermediate "$3"
fi
rm $before
rm $after
rm -f $intermediate
