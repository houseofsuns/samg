#!/usr/bin/env python3

import configparser
import dataclasses
import enum
import pathlib


class ThreatKind(enum.Enum):
    internal = 1
    red = 2
    white = 3
    blue = 4


class SoundKind(enum.Enum):
    alert = 1
    background_first_phase = 2
    background_first_phase_louder = 3
    background_first_phase_loudest = 4
    background_second_phase = 5
    background_second_phase_louder = 6
    background_second_phase_loudest = 7
    background_third_phase = 8
    background_third_phase_louder = 9
    background_third_phase_loudest = 10
    communication_down = 11
    communication_up = 12
    data_transfer = 13
    first_phase_ends_in_1_minute = 14
    first_phase_ends_in_20_seconds = 15
    incoming_data = 16
    internal_threat = 17
    operation_ends_in_1_minute = 18
    operation_ends_in_20_seconds = 19
    operation_end = 20
    operation_start = 21
    repeat = 22
    second_phase_ends_in_1_minute = 23
    second_phase_ends_in_20_seconds = 24
    serious_internal_threat = 25
    serious_threat_zone_blue = 26
    serious_threat_zone_red = 27
    serious_threat_zone_white = 28
    threat_zone_blue = 29
    threat_zone_red = 30
    threat_zone_white = 31
    time_t_plus_1 = 32
    time_t_plus_2 = 33
    time_t_plus_3 = 34
    time_t_plus_4 = 35
    time_t_plus_5 = 36
    time_t_plus_6 = 37
    time_t_plus_7 = 38
    time_t_plus_8 = 39
    transition_first_to_second_phase = 40
    transition_second_to_third_phase = 41
    unconfirmed_report = 42
    white_noise = 43


@dataclasses.dataclass
class Sound:
    path: pathlib.Path
    duration: float


class SoundRepository:
    def __init__(self, configpath=pathlib.Path('sounds.cfg')) -> None:
        config = configparser.SafeConfigParser()
        config.read(configpath)

        self.sounds = {}
        for x in config.sections():
            path = pathlib.Path(config.get(x, "path")).resolve()
            self.sounds[SoundKind[x]] = Sound(path,
                                              config.getfloat(x, "duration"))

    def __getitem__(self, key) -> Sound:
        return self.sounds.__getitem__(key)


# ending of stuff is always after 7 seconds in the sound snippet
# this is acurate enough
MARK = 7


def sectomin(seconds: float) -> str:
        return "{:2d}:{:02d}".format(int(seconds // 60), int(seconds % 60))


@dataclasses.dataclass
class Event:
    time: float

    def textify(self) -> str:
        return "%s  An Event\n" % (sectomin(self.time))


@dataclasses.dataclass
class Threat(Event):
    turn: int
    kind: ThreatKind
    unconfirmed: bool
    serious: bool

    def textify(self) -> str:
        ret = "{}  Zeit T+{} ".format(sectomin(self.time), self.turn)
        if self.unconfirmed:
            ret += "Unbestätigte "
        if self.serious:
            ret += "Ernsthafte "
        if self.kind == 'internal':
            ret += "Interne Bedrohung"
        else:
            ret += "Bedrohung in Zone "
            if self.kind == "blue":
                ret += "Blau"
            elif self.kind == "white":
                ret += "Weiß"
            elif self.kind == "red":
                ret += "Rot"
            else:
                raise RuntimeError("Unknown kind.")
        return ret + "\n"


@dataclasses.dataclass
class PhaseWarning(Event):
    phase: int
    countdown: int

    def textify(self) -> str:
        # correction for textify so that everything lines up nicely
        TRANSITION_CORRECTION = 3
        ret = "{}  ".format(sectomin(self.time + TRANSITION_CORRECTION))
        if self.phase == 1:
            ret += "Erste Phase endet in "
        elif self.phase == 2:
            ret += "Zweite Phase endet in "
        elif self.phase == 3:
            ret += "Operation endet in "
        else:
            raise RuntimeError("Wrong phase.")
        if self.countdown == 60:
            ret += "einer Minute"
        elif self.countdown == 20:
            ret += "zwanzig Sekunden"
        else:
            raise RuntimeError("Wrong countdown.")
        return ret + "\n"


@dataclasses.dataclass
class PhaseTransition(Event):
    fromphase: int

    def textify(self) -> str:
        if self.fromphase == 0:
            return "{}  Operation beginnt\n".format(sectomin(self.time))
        elif self.fromphase == 1:
            return "{}  Erste Phase endet und zweite Phase beginnt\n".format(
                sectomin(self.time + MARK))
        elif self.fromphase == 2:
            return "{}  Zweite Phase endet und dritte Phase beginnt\n".format(
                sectomin(self.time + MARK))
        elif self.fromphase == 3:
            return "{}  Operation endet\n".format(sectomin(self.time + MARK))
        else:
            raise RuntimeError("Wrong phase")


@dataclasses.dataclass
class CommFail(Event):
    duration: float

    def textify(self) -> str:
        msg = ("{}  Kommunikationssystem ausgefallen\n"
               "{}  Kommunikationssystem wiederhergestellt\n")
        return msg.format(sectomin(self.time),
                          sectomin(self.time + self.duration))


@dataclasses.dataclass
class IncomingData(Event):
    def textify(self) -> str:
        return "{}  Eingehende Daten\n".format(sectomin(self.time))


@dataclasses.dataclass
class DataTransfer(Event):
    def textify(self) -> str:
        return "{}  Datenübertragung\n".format(sectomin(self.time))
