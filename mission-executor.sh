#!/bin/bash
echo "Generate audio file"
echo "(takes ~100MB per minute track length temporary space)"
if [[ -d "/tmp/samg" ]]; then
    echo "Using /tmp/samg (which hopefully contains a tmpfs)"
    TMPDIR="/tmp/samg"
    export TMPDIR
fi
chmod +x mission.sh
./mission.sh
oggenc -o ./mission.ogg ./mission.wav
if [[ ! -f .serial ]]; then
    echo "0" > .serial
fi
SERIAL=$[$(cat .serial) + 1]
if [[ ! -d archive ]]; then
    mkdir archive
fi
for EXT in ogg txt json; do
    cp mission.$EXT archive/$SERIAL.$EXT
done
echo $SERIAL > .serial
echo "Created mission number $SERIAL"
