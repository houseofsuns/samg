#!/usr/bin/env python3

import argparse
import json
import pathlib
import string
from typing import (Dict, Any)

from common import (
    MARK, SoundRepository, Threat, CommFail, DataTransfer, IncomingData,
    PhaseTransition, PhaseWarning,
    SoundKind as SK)


def decodeJSON(dict_: Dict[str, Any]) -> Any:
    if "type" in dict_:
        if dict_["type"] == "threat":
            return Threat(dict_["time"], dict_["turn"], dict_["kind"],
                          dict_["unconfirmed"], dict_["serious"])
        elif dict_["type"] == "commfail":
            return CommFail(dict_["time"], dict_["duration"])
        elif dict_["type"] == "datatransfer":
            return DataTransfer(dict_["time"])
        elif dict_["type"] == "incomingdata":
            return IncomingData(dict_["time"])
        else:
            raise RuntimeError("Invalid type in JSON")
    else:
        return dict_


class Plan:
    def __init__(self, path: pathlib.Path,
                 repository: SoundRepository) -> None:
        with open(path) as f:
            data = json.load(f, object_hook=decodeJSON)
        self.phasedurations = {int(k): int(v)
                               for k, v in data["phasedurations"].items()}
        self.totalduration = sum(v for v in self.phasedurations.values())
        self.events = data["events"]
        if len(self.phasedurations) != 3:
            raise RuntimeError("Wrong number of durations")

        phasestart = 0
        phaseend = self.phasedurations[1]
        self.events.append(PhaseTransition(phasestart, 0))
        s = repository[SK.first_phase_ends_in_1_minute]
        self.events.append(PhaseWarning(phaseend - 60 - s.duration, 1, 60))
        s = repository[SK.first_phase_ends_in_20_seconds]
        self.events.append(PhaseWarning(phaseend - 20 - s.duration, 1, 20))
        phasestart += self.phasedurations[1]
        phaseend += self.phasedurations[2]
        self.events.append(PhaseTransition(phasestart - MARK, 1))
        s = repository[SK.second_phase_ends_in_1_minute]
        self.events.append(PhaseWarning(phaseend - 60 - s.duration, 2, 60))
        s = repository[SK.second_phase_ends_in_20_seconds]
        self.events.append(PhaseWarning(phaseend - 20 - s.duration, 2, 20))
        phasestart += self.phasedurations[2]
        phaseend += self.phasedurations[3]
        self.events.append(PhaseTransition(phasestart - MARK, 2))
        s = repository[SK.operation_ends_in_1_minute]
        self.events.append(PhaseWarning(phaseend - 60 - s.duration, 3, 60))
        s = repository[SK.operation_ends_in_20_seconds]
        self.events.append(PhaseWarning(phaseend - 20 - s.duration, 3, 20))
        self.events.append(PhaseTransition(phaseend - MARK, 3))

        self.events.sort(key=lambda x: x.time)


SAMPLERATE = 44100


TEMPLATES = {
    'base': """#!/bin/bash

intermediate=`tempfile --suffix '.wav'`
tempa=`tempfile --suffix '.wav'`
tempb=`tempfile --suffix '.wav'`

echo "Preparing Background"
${FILLBACKGROUND}
echo "Inserting Events"
${EVENTS}
echo "Cleaning up"
cp $intermediate ./mission.wav
rm $intermediate $tempa $tempb
echo "Sox finished"
""",

    'background': """
sox ${SIRENS} $intermediate
""",

    'trim': """
sox ${INPUT} ${OUTPUT} trim 0 ${POINT}s
${CONTINUATION}
""",

    'insert': """
./audio_replace.sh ${INPUT} ${SOUND} ${OUTPUT} ${POINT}
${CONTINUATION}
""",

    'verbatim': """
${LINE}
${CONTINUATION}
""",

    'noise': """
sox ${NOISES} ${OUTPUT}
${CONTINUATION}
""",
}

tempfiles = ("$tempa", "$tempb")


def tsubst(template: string.Template,
           **keys: Dict[str, Any]) -> string.Template:
    """Do template substitiution"""
    return string.Template(template.safe_substitute(keys))


class Scripter:
    def __init__(self, plan: Plan, repository: SoundRepository) -> None:
        self.plan = plan
        self.repository = repository
        self.script = string.Template(TEMPLATES['base'])

    def create_script(self) -> None:
        self.fill_background()
        print("Background finished, scripting events")
        self.do_mission()

    def update_script(self, template_name: str, parameter_name: str,
                      **kwargs: Dict[str, Any]) -> None:
        if "CONTINUATION" not in kwargs:
            kwargs["CONTINUATION"] = "${{{}}}".format(parameter_name)
        cmd = string.Template(TEMPLATES[template_name])
        cmd = cmd.safe_substitute(**kwargs)
        finalargs = {parameter_name: cmd}
        self.script = tsubst(self.script, **finalargs)

    def fill_background(self) -> None:
        point = 0.0
        phasenames = {1: 'first',  2: 'second', 3: 'third'}
        sirens = []
        while point < self.plan.totalduration:
            if point < self.plan.phasedurations[1]:
                phase = 1
                phasestart = 0
                phaseend = self.plan.phasedurations[1]
            elif (point < (self.plan.phasedurations[1]
                           + self.plan.phasedurations[2])):
                phase = 2
                phasestart = self.plan.phasedurations[1]
                phaseend = (self.plan.phasedurations[1]
                            + self.plan.phasedurations[2])
            else:
                phase = 3
                phasestart = (self.plan.phasedurations[1]
                              + self.plan.phasedurations[2])
                phaseend = self.plan.totalduration
            if point < (phasestart + phaseend) / 3:
                intensity = ""
            elif point < 2 * (phasestart + phaseend) / 3:
                intensity = "_louder"
            else:
                intensity = "_loudest"
            sound = self.repository[SK["background_{}_phase{}".format(
                phasenames[phase], intensity)]]
            sirens.append(sound.path)
            point += sound.duration
        self.update_script('background', "FILLBACKGROUND",
                           SIRENS=" ".join(map(str, sirens)))

    def do_mission(self) -> None:
        lasttime = -1
        with open('./mission.txt', 'w') as f:
            repo = self.repository
            for event in plan.events:
                f.write(event.textify())
                if event.time < lasttime:
                    raise RuntimeError("Overlap detected at {}".format(event))
                sound = None
                if isinstance(event, PhaseTransition):
                    sounds = {
                        0: repo[SK.operation_start],
                        1: repo[SK.transition_first_to_second_phase],
                        2: repo[SK.transition_second_to_third_phase],
                        3: repo[SK.operation_end],
                    }
                    sound = sounds[event.fromphase]
                elif isinstance(event, PhaseWarning):
                    sounds = {
                        (1, 60): repo[SK.first_phase_ends_in_1_minute],
                        (1, 20): repo[SK.first_phase_ends_in_20_seconds],
                        (2, 60): repo[SK.second_phase_ends_in_1_minute],
                        (2, 20): repo[SK.second_phase_ends_in_20_seconds],
                        (3, 60): repo[SK.operation_ends_in_1_minute],
                        (3, 20): repo[SK.operation_ends_in_20_seconds],
                    }
                    sound = sounds[(event.phase, event.countdown)]
                elif isinstance(event, Threat):
                    lasttime = self.do_threat(event)
                elif isinstance(event, CommFail):
                    lasttime = self.do_commfail(event)
                elif isinstance(event, DataTransfer):
                    sound = repo[SK.data_transfer]
                elif isinstance(event, IncomingData):
                    sound = repo[SK.incoming_data]
                else:
                    raise RuntimeError("Invalid Event")
                if sound:
                    self.update_script(
                        'insert', "EVENTS", INPUT="$intermediate",
                        SOUND=sound.path, OUTPUT=tempfiles[0],
                        POINT=int(event.time*SAMPLERATE))
                    self.update_script(
                        'verbatim', "EVENTS",
                        LINE="cp {} $intermediate".format(tempfiles[0]))
                    lasttime = event.time + sound.duration
        self.script = self.script.safe_substitute(EVENTS="")
        self.script = "\n".join(line for line in self.script.split("\n")
                                if line.strip())
        with open('./mission.sh', 'w') as f:
            f.write(self.script)

    def do_threat(self, threat):
        sounds = []
        sounds.append(self.repository[SK.alert])
        if threat.serious:
            prefix = "serious_"
        else:
            prefix = ""
        if threat.unconfirmed:
            sounds.append(self.repository[SK.unconfirmed_report])
        sounds.append(
            self.repository[SK["time_t_plus_{}".format(threat.turn)]])
        if threat.kind == "internal":
            sounds.append(
                self.repository[SK["{}internal_threat".format(prefix)]])
        else:
            k = SK["{}threat_zone_{}".format(prefix, threat.kind)]
            sounds.append(self.repository[k])
        sounds.append(self.repository[SK.repeat])
        if threat.unconfirmed:
            sounds.append(self.repository[SK.unconfirmed_report])
        sounds.append(
            self.repository[SK["time_t_plus_{}".format(threat.turn)]])
        if threat.kind == "internal":
            sounds.append(
                self.repository[SK["{}internal_threat".format(prefix)]])
        else:
            k = SK["{}threat_zone_{}".format(prefix, threat.kind)]
            sounds.append(self.repository[k])
        curtime = threat.time
        infile = "$intermediate"
        outfile = tempfiles[0]
        counter = 1
        for sound in sounds:
            self.update_script(
                'insert', "EVENTS", INPUT=infile, SOUND=sound.path,
                OUTPUT=outfile, POINT=int(curtime*SAMPLERATE))
            infile = outfile
            outfile = tempfiles[counter % 2]
            counter += 1
            curtime += sound.duration + 0.05
        self.update_script(
            'verbatim', "EVENTS", LINE="cp {} $intermediate".format(infile))
        return curtime

    def do_commfail(self, commfail):
        curtime = commfail.time
        noise = self.repository[SK.white_noise]
        num = int(commfail.duration // noise.duration) + 2
        self.update_script(
            'noise', "EVENTS", NOISES=" ".join((str(noise.path),)*num),
            OUTPUT=tempfiles[0])
        self.update_script(
            'trim', "EVENTS", INPUT=tempfiles[0], OUTPUT=tempfiles[1],
            POINT=int(commfail.duration*SAMPLERATE))
        self.update_script(
            'insert', "EVENTS", INPUT="$intermediate",
            SOUND=self.repository[SK.communication_down].path,
            OUTPUT=tempfiles[0], POINT=int(curtime*SAMPLERATE))
        curtime += self.repository[SK.communication_down].duration + 0.05
        self.update_script(
            'insert', "EVENTS", INPUT=tempfiles[0], SOUND=tempfiles[1],
            OUTPUT="$intermediate", POINT=int(curtime*SAMPLERATE))
        curtime += commfail.duration
        self.update_script(
            'insert', "EVENTS", INPUT="$intermediate",
            SOUND=self.repository[SK.communication_up].path,
            OUTPUT=tempfiles[0], POINT=int(curtime*SAMPLERATE))
        curtime += self.repository[SK.communication_up].duration
        self.update_script(
            'verbatim', "EVENTS",
            LINE="cp {} $intermediate".format(tempfiles[0]))
        return curtime


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description='Write a shell script actually creating the mission.')
    parser.add_argument('missionpath', default='./mission.json',
                        help='path/to/mission/file', nargs='?')
    args = parser.parse_args()
    print("Initialize scripter")
    repository = SoundRepository()
    plan = Plan(args.missionpath, repository)
    scripter = Scripter(plan, repository)
    print("Building script file")
    scripter.create_script()
    print("Finished scripting")
